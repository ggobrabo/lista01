import java.util.Scanner;

public class Exercicio09 {
    public static void executar() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Digite o valor de A: ");
        int a = scanner.nextInt();

        System.out.print("Digite o valor de B: ");
        int b = scanner.nextInt();

        if (a == b) {
            System.out.println("Os números são iguais.");
        } else {
            System.out.println("Os números são diferentes.");
            int maior = Math.max(a, b);
            System.out.println("O maior número é: " + maior);
        }

        scanner.close();
    }
}
