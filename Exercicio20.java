import java.util.Scanner;

public class Exercicio20 {
    public static void executar() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Digite o tempo da viagem em horas: ");
        double tempoViagemHoras = scanner.nextDouble();

        System.out.print("Digite a velocidade média da viagem em km/h: ");
        double velocidadeMediaKmPorHora = scanner.nextDouble();

        double distanciaTotalKm = tempoViagemHoras * velocidadeMediaKmPorHora;
        double litrosGastos = distanciaTotalKm / 12; // Um carro que faz 12 km por litro

        System.out.println("A quantidade de litros de combustível gasta na viagem é: " + litrosGastos);

        scanner.close();
    }
}
